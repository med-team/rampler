Source: rampler
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Pranav Ballaney <ballaneypranav@gmail.com>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               cmake,
               libbioparser-dev
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/med-team/rampler
Vcs-Git: https://salsa.debian.org/med-team/rampler.git
Homepage: https://github.com/rvaser/rampler
Rules-Requires-Root: no

Package: rampler
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: module for sampling genomic sequences
 Standalone module for sampling genomic sequences. It supports two modes,
 random subsampling of sequencer data to a desired depth (given the
 reference length) and file splitting to desired size in bytes.
 .
 Rampler takes as first input argument a file in FASTA/FASTQ format which
 can be compressed with gzip. The rest of input parameters depend on the
 mode of operation. The output is stored into a file(s) which is in the
 same format as the input file but uncompressed.
